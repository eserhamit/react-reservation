import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Contact from './Components/Contact';
import InfoVoyage from './Components/InfoVoyage';
import Reservation from './Components/Reservation';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Error from './Components/PageError';
import Navbar from './Components/Navbar';

ReactDOM.render(
  <React.StrictMode>
    <Navbar />
    <Router>     
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/contact" component={Contact} />
        <Route path="/infoVoyage" component={InfoVoyage} />
        <Route path="/reservation" component={Reservation} />
        <Route component={Error} />
      </Switch>
    </Router>  
  </React.StrictMode>,
  document.getElementById('root')
);

