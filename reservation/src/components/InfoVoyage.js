import React, { useState } from 'react'

const Page1 = () => {
  let dataVoyageur = localStorage.getItem('voyageur')
  let dataVoyage = localStorage.getItem('voyage')

  const [getVoyageur, setgetVoyageur] = useState(JSON.parse(dataVoyageur))
  const [getVoyage, setgetVoyage] = useState(JSON.parse(dataVoyage))




  return (
    <div className="container">
<h1 className="text-center">Information de Voyage</h1>
      <ul className="list-group list-group-flush">
 
          <li className="list-group-item"><strong>Pays Depart :</strong>{ getVoyage.paysDepart}</li>
          <li className="list-group-item"><strong>Pays Retour :</strong> { getVoyage.paysRetour}</li>
          <li className="list-group-item"><strong>Date Depart :</strong>{ getVoyage.depart}</li>
          <li className="list-group-item"><strong>Date Retour :</strong> { getVoyage.retour}</li>


      
      </ul>


      <table className="table table-hover">
        <thead className="bg-warning">
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
          {getVoyageur.map((voyageur, i) => (
            <tr key={i}>
              <th scope="row">Voyageur {voyageur.id+1}</th>
              <td>{voyageur.nom}</td>
              <td>{voyageur.prenom}</td>
              <td>{voyageur.dateNaissance}</td>
            </tr>
          ))}
        </tbody>
      </table>


    </div>
  )
}

export default Page1


