import React from 'react';

// Create component for button
class Button extends React.Component {
    render() {
      return (       
          <button
            type={this.props.type || 'button'}
            value={this.props.value || null}
            className={this.props.className || null}
            onClick={this.props.onClick}
          >
            {this.props.text}
          </button>
      );
    }
  };

export default Button;
