import React from 'react';
import Label from './Label'

// Create component for select input
class Select extends React.Component {
    render() {
      // Get all options from option prop
      const selectOptions = this.props.options;
  
      ;
  
      return (
        <>
          <Label
            hasLabel={this.props.hasLabel}
            htmlFor={this.props.htmlFor}
            label={this.props.label}
            classLabel={this.props.classLabel}
          />
          <select
            defaultValue=''
            id={this.props.htmlFor}
            name={this.props.name || null}
            className={this.props.className || null}
            required={this.props.required || null}
          >
            <option value='' disabled>Select one option</option>
  
            {
            console.log(selectOptions)
            }
          </select>
        </>
      );
    }
  };
export default Select;
  