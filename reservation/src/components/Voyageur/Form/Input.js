import React from 'react';
import Label from './Label'

 
// Create component for input
class Input extends React.Component {
    render() {
      return (
        <>
          <Label
            hasLabel={this.props.hasLabel}
            htmlFor={this.props.htmlFor}
            label={this.props.label}
            classLabel={this.props.classLabel}
          />
            <input
                id={this.props.htmlFor}
                max={this.props.max || null}
                min={this.props.min || null}
                name={this.props.name || null}
                value={this.props.value || ''}
                onChange={this.props.onChange || null}

                placeholder={this.props.placeholder || null}
                step={this.props.step || null}
                className={this.props.className || null}
                type={this.props.type || 'text'}
                required={this.props.required || null}
            />
        </>
      );
    }
  }
export default Input;
