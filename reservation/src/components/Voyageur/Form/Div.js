import React from 'react';

// Create component for label
class Div extends React.Component {
    render() {
      if (this.props.hasDiv === 'true') {
        return <div className="col-sm-3" htmlFor={this.props.htmlFor}>{this.props.label}</div>
      }else {
        return ''
      }
    }
  } 
export default Div;