import React from 'react';

// Create component for label
class Label extends React.Component {
    render() {
      if (this.props.hasLabel === 'true') {
        return <label className={this.props.classLabel} htmlFor={this.props.htmlFor}>{this.props.label}</label>
      }else {
        return ''
      }
    }
  } 
export default Label;