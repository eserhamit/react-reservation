import React, { useState, useEffect } from 'react';

import Input from './Input';
import Button from './Button';
import Label from './Label';
import Voyageur from '../Voyageurs'
import Pays from './Pays'
import FormValidation from '../FormValidation';

const Form = () => {
    let today = new Date().toISOString().slice(0, 10)

    const [voyageurLastId, setVoyageurLastId] = useState(0)
    const [listeVoyageurs, setListeVoyageurs] = useState([])
    const [voyageType, setVoyageType] = useState(true)
    const [values, setValues] = useState({});
    const [errors, setErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [dateDepart, setdateDepart] = useState(today)

    const {
        validate,
    } = FormValidation();

    const handleChange = (event) => {
        if (event.target.name === 'depart' && voyageType === true) setdateDepart(event.target.value)
        event.persist();
        setValues(value => ({ ...value, [event.target.name]: event.target.value }));

    }

    const handleChangeVoyageur = i => (event) => {
        event.persist();
        const newVoyageur = { ...listeVoyageurs[i], [event.target.name]: event.target.value };
        const newListeVoyageurs = [...listeVoyageurs];
        newListeVoyageurs[i] = newVoyageur;
        setListeVoyageurs(newListeVoyageurs)

    }
    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
            alert('valid')
            localStorage.setItem('voyage', JSON.stringify(values))
            localStorage.setItem('voyageur', JSON.stringify(listeVoyageurs))
            document.getElementById('form').submit()
        }

    }, [errors]);

    const handleSubmit = (event) => {
        if (event) event.preventDefault();
        setErrors(validate(values, listeVoyageurs))
        setIsSubmitting(true);
        return false;
    }

    const getNextId = () => {
        const id = voyageurLastId
        setVoyageurLastId(id + 1)
        return id
    }

    const handleAddVoyageur = event => {
        event.preventDefault()
        const nouveauVoyageur = {
            id: getNextId(),
        };
        const nouveauListeVoyageurs = [...listeVoyageurs];
        nouveauListeVoyageurs.push(nouveauVoyageur);
        setListeVoyageurs(nouveauListeVoyageurs);
    }

    const handleDeleteVoyageur = i => () => {
        const voyageursCopy = [...listeVoyageurs]
        console.log("delete", i)
        voyageursCopy.splice(i, 1)
        setListeVoyageurs(voyageursCopy)
    }

    const voyageursDiv = listeVoyageurs.map((voyageur, i) => {
        return <Voyageur
            key={voyageur.id}
            onDelete={handleDeleteVoyageur(i)}
            onChange={handleChangeVoyageur(i)}
            voyageur={voyageur}
            errors={errors}
        />
    })
    return (
        <div className="container">
            <div className="row">
                <form method="" action='infoVoyage' id="form" style={{ minWidth: '90%' }}>
                    <div className="header text-center">
                        <h2>Formulaire de reservation</h2>
                    </div>
                    <div className="row">
                        <a href="# " className="btn btn-warning mr-1" onClick={() => { setVoyageType(true) }}>Aller-retour</a>
                        <a href="# " className="btn btn-warning" onClick={() => { setVoyageType(false) }}>Aller simple</a>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm-12 col-md-6">
                            <Label
                                hasLabel='true'
                                htmlFor='pays'
                                label="D'où partez-vous"
                            />
                            <select className='form-control' value={values.paysDepart} name="paysDepart" onChange={handleChange}>
                                <Pays />
                            </select>
                            <span className="error">{errors.paysDepart}</span>
                        </div>
                        <div className="form-group col-sm-12 col-md-6">
                            <Label
                                hasLabel='true'
                                htmlFor='pays'
                                label='Où allez-vous'
                            />
                            <select className='form-control' value={values.paysRetour} name="paysRetour" onChange={handleChange}>
                                <Pays />
                            </select>
                            <span className="error">{errors.paysRetour}</span>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="form-group col-sm-12 col-md-6">
                            <Input
                                hasLabel='true'
                                htmlFor='depart'
                                label='Départ'
                                type='date'
                                name="depart"
                                min={today}
                                value={values.depart}
                                onChange={handleChange}
                                className='form-control '
                            />
                            <span className="error">{errors.depart}</span>
                        </div>
                        {
                            voyageType ?
                                <div className="form-group col-sm-12 col-md-6">
                                    <Input
                                        hasLabel='true'
                                        htmlFor='retour'
                                        label='Retour'
                                        type='date'
                                        name="retour"
                                        min={dateDepart}
                                        value={values.retour}
                                        onChange={handleChange}
                                        className='form-control '
                                    />
                                    <span className="error">{errors.retour}</span>
                                </div> : null
                        }
                    </div>
                    <div className="form-group">
                        <Button
                            text='Add Voyageur'
                            className='btn btn-primary '
                            onClick={handleAddVoyageur}
                        />
                    </div>

                    <div id="voyageur"> {voyageursDiv} </div>
                    {
                        listeVoyageurs.length ?
                            <div className="form-group">
                                <button
                                    className='btn btn-primary '
                                    onClick={handleSubmit}
                                >Rechercher des vols </button>
                            </div> : null
                    }
                </form>
            </div>
        </div>
    )
}
export default Form;
