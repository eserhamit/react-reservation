import React from 'react';



function Voyageur(props) {


    const handleDelete = event => {
        event.preventDefault()
        if (props.onDelete) {
            props.onDelete()
        }
    }

    return (
        <div className="form-group row">
            <div className="form-group col-sm-12 col-md-4">
                <input
                    type='text'
                    name='nom'
                    placeholder="Nom"
                    className='form-control '
                    value={props.voyageur.nom}
                    onChange={props.onChange}
                />
                {props.voyageur.nom ? null : <span className="error">{props.errors.nom}</span>}
            </div>
            <div className="form-group col-sm-12 col-md-4">
                <input
                    type='text'
                    name='prenom'
                    placeholder="Prènom"
                    className='form-control '
                    value={props.voyageur.prenom}
                    onChange={props.onChange}
                />
                {props.voyageur.prenom ? null : <span className="error">{props.errors.prenom}</span>}
            </div>
            <div className="form-group col-sm-12 col-md-3">
                <input
                    type='date'
                    name='dateNaissance'
                    className='form-control '
                    value={props.voyageur.dateNaissance}
                    onChange={props.onChange}
                />
                {props.voyageur.dateNaissance ? null : <span className="error">{props.errors.dateNaissance}</span>}
            </div>
            <div className="form-group col-sm-12 col-md-1">
                <button className="btn btn-danger" onClick={handleDelete}>Delete</button>
            </div>
        </div>
    );
}

export default Voyageur;
