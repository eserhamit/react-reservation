const FormValidation = () => {

    const validate = (values, listeVoyageurs) => {

        let errors = {};

        if(!values.paysDepart) {
            errors.paysDepart = 'Veuillez saisir un pays depart';
        }

        if(!values.paysRetour) {
            errors.paysRetour = 'Veuillez saisir un pays retour';
        }

        if(!values.depart) {
            errors.depart = 'Veuillez saisir une date de depart';
        }
        if(!values.retour) {
            errors.retour = 'Veuillez saisir une date retour';
        }

        listeVoyageurs.map((voyageur, i) => { 
            if(!voyageur.prenom) {
                errors.prenom = `Veuillez saisir un prènom ${voyageur.id}`;
            }
            if(!voyageur.nom) {
                errors.nom = 'Veuillez saisir un nom';
            }
            if(!voyageur.dateNaissance) {
                errors.dateNaissance = 'Veuillez saisir une date';
            }
            return errors;

        })

        

        return errors;
    }

    return {
        validate
    }
 
}
export default FormValidation;