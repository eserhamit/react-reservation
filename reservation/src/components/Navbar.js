import React from 'react'
import logo from '../images/logo.jpg'

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="# ">
                <img src={logo} width="200px"/>
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="http://localhost:3000/">Home <span className="sr-only">(current)</span></a>
                    </li>
                    
                    <li className="nav-item">
                        <a className="nav-link" href="reservation">Reservation</a>
                    </li>
                    
                    <li className="nav-item">
                        <a className="nav-link" href="contact">contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Navbar
